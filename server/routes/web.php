<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\MyTestController;

use App\Models\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $users = DB::table('authors')->distinct()->get();
//    dump($users);

//    return view('blocks.contents.home');
});

Route::get('/category/{id}', function () {
    return view('blocks.contents.category');
});

Route::get('/article/{id}', function () {
    return view('blocks.contents.article');
});

//Route::get('/author/{id}', function ($id) {
//    return $id;
//});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

