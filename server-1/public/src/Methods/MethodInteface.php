<?php

namespace ZabaraIndustry\Loggder\Methods;

interface MethodInterface
{
    public function writeLog($level, $message);
}
